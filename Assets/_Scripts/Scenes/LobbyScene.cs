﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LobbyScene : MonoBehaviour
{
	[SerializeField] private TMP_InputField createUsername;
	[SerializeField] private TMP_InputField createPassword;
	[SerializeField] private TMP_InputField createEmail;
	[SerializeField] private TMP_InputField loginUsernameOrEmail;
	[SerializeField] private TMP_InputField loginPassword;
	[SerializeField] private TextMeshProUGUI welcomeMsg;
	[SerializeField] private TextMeshProUGUI authenticationMsg;
	[SerializeField] private CanvasGroup canvas;

	public static LobbyScene Instance { set; get; }

	void Start()
	{
		Instance = this;
	}

	public void OnCLickCreateAccount()
	{
		DisableInputs();

		string username = createUsername.text;
		string password = createPassword.text;
		string email = createEmail.text;

		Client.Instance.SendCreateAccount(username, password, email);
	}
	public void OnClickLoginRequest()
	{
		DisableInputs();

		string username = loginUsernameOrEmail.text;
		string password = loginPassword.text;

		Client.Instance.SendLoginRequest(username, password);
	}

	public void ChangeWelcomeMessage(string msg)
	{
		welcomeMsg.text = msg;
	}
	public void ChangeAuthenticationMessage(string msg)
	{
		authenticationMsg.text = msg;
	}

	public void EnableInputs()
	{
		canvas.interactable = true;
	}
	public void DisableInputs()
	{
		canvas.interactable = false;
	}
}
